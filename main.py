#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Importamos las clases además de librería que serán utiles
import pandas as pd
from Pelicula import Pelicula
from Filtro import Filtro
from Busqueda import Busqueda


# Funcion que carga el archivo y lo convierte en una matriz
def cargar_archivo():
    data = pd.read_csv("IMDb movies.csv")
    return data


# Funcion que nos permite obtener los indices de las peliculas
def obtener_indices(archivo, pelicula):
    pelis = Busqueda(archivo["title"])
    # print(pelis)
    pelis_todas = pelis.get_listas()
    indice = pelis_todas.index(pelicula)
    # Nos retorna el indice que queremos de la pelicula ingresada
    return indice


# Funcion que nos sirve para escribir la pelicula con todos sus datos
def escribir_pelicula(pelicula):
    print("\n\nEl titulo de su pelicula es {0} la cual fue estrenada en el año"
          " {1} con el/los generos de {2}, la pelicula fue dirigida por {3}"
          " con un reparto de los siguientes actores {4}. La pelicula dura"
          " {5} minutos con una valoracion de {6}. Una breve descripcion de la"
          " pelicula es {7}".format(pelicula.get_titulo(),
                                    pelicula.get_fecha(),
                                    pelicula.get_genero(),
                                    pelicula.get_director(),
                                    pelicula.get_actores(),
                                    pelicula.get_duracion(),
                                    pelicula.get_valoracion(),
                                    pelicula.get_descripcion()))


# Funcion que busca la pelicula luego de aplicar el filtro
def buscar_pelicula(archivo, lista):
    # Pedimos la pelicula
    contador = 0
    pelicula = str(input("\n\nIngrese la pelicula que desea saber más: \n"))
    pelicula = pelicula.title()
    # Recorremos el filtro
    for i in lista:
        # Generamos la condicion
        if i == pelicula:
            # Obtenemos el indice de la pelicula y hacemos un objeto a la
            # pelicula además definimos cada uno de los atributos que nos
            # interesan sobre la pelicula
            index = obtener_indices(archivo, pelicula)
            pelicula = Pelicula(index)
            pelicula.set_titulo(archivo)
            pelicula.set_fecha(archivo)
            pelicula.set_genero(archivo)
            pelicula.set_actores(archivo)
            pelicula.set_valoracion(archivo)
            pelicula.set_director(archivo)
            pelicula.set_duracion(archivo)
            pelicula.set_descripcion(archivo)
            contador = 10

    # Si encontro una pelicula preguntamos si desea realizar otra busqueda
    if contador == 10:
        # Escribimos la pelicula pasada
        escribir_pelicula(pelicula)
        indicador = str(input("\nSi desea buscar una nueva pelicula, presione"
                              " 1, de lo contrario presione 2.\n"))

        if indicador == "1":
            main()
        elif indicador == "2":
            print("Muchas gracias por utilizar nuestra app de busqueda"
                  ".Que tenga buena tarde")
        else:
            print("Ud ingresó un dato no valido por lo tanto asumimos que no"
                  " desea seguir utilizando la app. Adiós")

    else:
        print("Su pelicula no fue encontrada en los filtros estipulados o "
              "se equivocó al escribir la misma, se le devolverá al menú "
              "principal")
        main()


# Funcion principal
def main():
    # Cargamos el dataset
    archivo = cargar_archivo()
    # Impresión inicial
    print("╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬")
    print("╬╬╬╬╬ Bienvenidos a la biblioteca de películas M&M ╬╬╬╬╬")
    print("╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬")
    print("Si desea buscar una pelicula por su genero, presione 1\n")
    print("Si desea buscar una pelicula por su año, presione 2\n")
    print("Si desea buscar una pelicula por su idioma, presione 3\n")
    # Pedimos la opción de tipo númerico
    pedido = input("Ingrese su opcion: ")
    # Ocupamos try and except
    try:
        # Transformamos el numero a entero y comparamos
        pedido = int(pedido)
        # En caso de que sea el filtro por genero
        if pedido == 1:
            # Se pide el genero en ingles y se realiza el filtro con aquel
            gen = str(input("\nIngrese el genero para buscar peliculas (El"
                            " genero debe ser escrito en inglés): \n"))
            gen = gen.capitalize()
            filtro = Filtro(archivo, gen)
            # Se extraen todas las películas con el genero pedido
            filtro_genero = filtro.get_genero()
            # Realizamos esto para asegurarnos que existió un filtro sino
            # nos devuelve al principio
            if len(filtro_genero) != 0:
                print("\n\nLas peliculas con el genero elegido son:\n")
                for i in filtro_genero:
                    print(i)
                # LLamamos la funcion para buscar una pelicula en el genero
                # solicitado
                buscar_pelicula(archivo, filtro_genero)
            else:
                print("Ud ingreso un genero no valido")
                main()
        # En caso de que el filtro sea por año
        elif pedido == 2:
            # Pedimos el año y realizamos el filtro
            anio = int(input("\nIngrese el año para buscar peliculas: \n"))
            filtro = Filtro(archivo, anio)
            # Obtenemos todas las peliculas en ese año
            filtro_years = filtro.get_year()
            # Nos aseguramos de que existió un filtro
            if len(filtro_years) != 0:
                print("\n\nLas peliculas en el año elegido son: \n")
                for i in filtro_years:
                    print(i)
                # Llamamos la funcion para buscar una pelicula en el año
                # solicitado
                buscar_pelicula(archivo, filtro_years)
            else:
                print("Ud ingresó un año no valido")
                main()
        # En caso de que el filtro sea por idioma
        elif pedido == 3:
            # Pedimos el idioma y realizamos el filtro
            idioma = str(input("\nIngrese el idioma de busqueda (El idioma "
                               "debe estar escrito en inglés): \n"))
            idioma = idioma.capitalize()
            filtro = Filtro(archivo, idioma)
            # Obtenemos todas las peliculas con ese idioma
            filtro_idioma = filtro.get_idioma()
            # Nos aseguramos de que existió un filtro adecuado sino nos
            # regresa al principio
            if len(filtro_idioma) != 0:
                print("\n\nLas peliculas con el idioma elegido son:\n")
                for i in filtro_idioma:
                    print(i)
                # Buscamos la pelicula requerida
                buscar_pelicula(archivo, filtro_idioma)
            else:
                print("Ud ingresó un idioma no valido")
                main()
        # Si no ingresó un numero de los solicitados se devuelve al principio
        else:
            print("INGRESE UNA OPCION VALIDA\n\n")
            main()
    # Si el usuario ingresó caracteres nos validos al principio
    except ValueError:
        print("POR FAVOR INGRESAR UN VALOR\n\n")
        main()


if __name__ == '__main__':
    main()
