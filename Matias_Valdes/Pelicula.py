#!/usr/bin/env python3
# -*- coding:utf-8 -*-


# Creacion de clase pelicula
class Pelicula():
    # Constructor por parametros
    def __init__(self, index):
        self.index = index
        self.__titulo = None
        self.__fecha = None
        self.__valoracion = None
        self.__genero = None
        self.__director = None
        self.__actores = None
        self.__duracion = None
        self.__descripcion = None

    # Metodos set sobre los datos que mas nos interesan sobre cada una de
    # las peliculas, esto se hace trabajando el dataset como una matriz
    def set_titulo(self, archivo):
        self.__titulo = archivo["title"][self.index]

    def set_fecha(self, archivo):
        self.__fecha = archivo["year"][self.index]

    def set_valoracion(self, archivo):
        self.__valoracion = archivo["avg_vote"][self.index]

    def set_director(self, archivo):
        self.__director = archivo["director"][self.index]

    def set_actores(self, archivo):
        self.__actores = archivo["actors"][self.index]

    def set_duracion(self, archivo):
        self.__duracion = archivo["duration"][self.index]

    def set_genero(self, archivo):
        self.__genero = archivo["genre"][self.index]

    def set_descripcion(self, archivo):
        self.__descripcion = archivo["description"][self.index]

    # Metodos get sobre la pelicula para obtener todos los datos de la
    # pelicula que creimos convenientes
    def get_genero(self):
        return self.__genero

    def get_descripcion(self):
        return self.__descripcion

    def get_director(self):
        return self.__director

    def get_comentario(self):
        return self.__comentario

    def get_titulo(self):
        return self.__titulo

    def get_fecha(self):
        return self.__fecha

    def get_valoracion(self):
        return self.__valoracion

    def get_actores(self):
        return self.__actores

    def get_duracion(self):
        return self.__duracion
