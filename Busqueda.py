#!/usr/bin/env python3
# -*- coding:utf-8 -*-


# Se crea la clase de busqueda
class Busqueda():
    # Constructor por parametros
    def __init__(self, busqueda):
        self.__busqueda = busqueda

    # Metodo para obtener listas
    def get_listas(self):
        # Crear lista en donde se agregará todos los posibles datos pedidos
        busqueda = []
        # Ciclo para añadir a la lista los datos de self.__busqueda
        # que corresponden al dato del archivo.csv pedido
        for i in self.__busqueda:
            busqueda.append(i)
        # Retorno de lista
        return busqueda
