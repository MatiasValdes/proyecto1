#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from Genero import Genero


class Filtro():

    def __init__(self, base, pedido):
        self.__base = base
        self.__pedido = pedido
        self.titulo = base["title"]

    def get_genero(self):
        objeto = Genero(self.__base["genre"])
        lista_generos = objeto.get_listas()

        for i in range(len(lista_generos)):
            if self.__pedido in lista_generos[i]:
                print(self.titulo.iloc[i])

    def get_year(self):
        objeto = Genero(self.__base["year"])
        lista_years = objeto.get_listas()

        for i in range(len(lista_years)):
            if self.__pedido == lista_years[i]:
                print(self.titulo.iloc[i])

    def get_idioma(self):
        objeto = Genero(self.__base["language"])
        lista_idiomas = objeto.get_listas()

        for i in range(len(lista_idiomas)):
            if self.__pedido == lista_idiomas[i]:
                print(self.titulo.iloc[i])
