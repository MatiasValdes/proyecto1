#! usr/bin/env python3
# -*- coding:utf-8 -*-

import pandas as pd
from Filtro import Filtro


def Datos():
    database = pd.read_csv("data.csv")
    return database


if __name__ == "__main__":
    database = Datos()
    print("Menú:")
    print("1.-Genero")
    print("2.-Año")
    print("3.-Idioma")
    opcion = int(input(":"))
    if opcion == 1:
        pedido = str(input("ingrese:"))
        pedido = pedido.capitalize()

        x = Filtro(database, pedido)
        x.get_genero()

    if opcion == 2:
        pedido = int(input("ingrese:"))

        x = Filtro(database, pedido)
        x.get_year()

    if opcion == 3:
        pedido = str(input("ingrese:"))
        pedido = pedido.capitalize()

        x = Filtro(database, pedido)
        x.get_idioma()

    else:
        exit()


