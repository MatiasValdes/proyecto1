#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from Busqueda import Busqueda


# Se crea la clase filtro que nos hará los filtros que hemos elegido
class Filtro():

    def __init__(self, base, pedido):
        # Variales a utilizar dentro de la clase
        self.__base = base
        self.__pedido = pedido
        self.titulo = base["title"]
        self.generos = []
        self.year = []
        self.idiomas = []

    # Metodo para conseguir una lista con filtro de busqueda
    # pedido(self.__pedido)
    def get_genero(self):
        # crear objeto mediante clase Busqueda.py
        objeto = Busqueda(self.__base["genre"])
        lista_generos = objeto.get_listas()

        # Ciclo de recorrido sobre una lista debido a que los datos de
        # archivo.py(genre) se agruparon en lista, porque tenian mas de
        # un valor
        for i in range(len(lista_generos)):
            if self.__pedido in lista_generos[i]:
                self.generos.append(self.titulo.iloc[i])

        # Retornar lista de peliculas con los generos pedidos
        return self.generos

    # Metodo para conseguir el una lista con filtro de
    # busqueda pedido(self.__pedido)
    def get_year(self):
        # Crear objeto mediante clase Genero.py
        objeto = Busqueda(self.__base["year"])
        lista_years = objeto.get_listas()

        # Ciclo de recorrido sobre una variable de tipo int los datos de
        # archivo.py(anio) al ser años se tomaron como type(int)
        for i in range(len(lista_years)):
            if self.__pedido == lista_years[i]:
                self.year.append(self.titulo.iloc[i])

        # Retornar lista de peliculas con los generos pedidos
        return self.year

    # Metodo para conseguir el una lista con filtro de busqueda
    # pedido(self.__pedido)
    def get_idioma(self):
        # Crear objeto mediante clase Genero.py
        objeto = Busqueda(self.__base["language"])
        lista_idiomas = objeto.get_listas()

        # Ciclo de recorrido sobre una variable de tipo str
        # los datos de archivo.py(titulo) al ser idiomas se tomaron como
        # type(str)
        for i in range(len(lista_idiomas)):
            if self.__pedido == lista_idiomas[i]:
                self.idiomas.append(self.titulo.iloc[i])

        # Retornar lista de peliculas con los generos pedidos
        return self.idiomas
